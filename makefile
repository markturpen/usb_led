all: a.exe

INC_PATH="libusb-1.0.26-binaries/libusb-MinGW-x64/include/"
LIB_PATH="libusb-1.0.26-binaries/VS2015-x64/lib/"



a.exe: main.c makefile
# 	cl /std:c17 /I$(INC_PATH) main.c $(LIB_PATH) /link /out:a.exe 
	clang -std=c17 -I$(INC_PATH) -L $(LIB_PATH) main.c 

run: a.exe
	a.exe

clean:
	rm *.obj *.exe